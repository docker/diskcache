FROM igwn/base:el9

ARG DISKCACHE_VERSION="2.7.7"

LABEL org.igwn.service="diskcache"
LABEL org.igwn.packager="Duncan Macleod <duncan.macleod@ligo.org>"
LABEL version="${DISKCACHE_VERSION}"
LABEL description="LDAS Tools Diskcache Service"

RUN dnf -y install \
      ldas-tools-diskcacheAPI-${DISKCACHE_VERSION} \
    && dnf clean all

CMD ["/usr/bin/diskcache", "daemon", "--configuration-file", "/etc/diskcache/diskcache.rsc"]
